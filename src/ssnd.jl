module ssnd

export Settings, read_network_file, design_network, init_network

import Base.show

"""
    Settings(commercial_diameters::Array{Float64,1}
             v_min::Float64
             v_max::Float64
             n::Float64
             roughness::Float64
             delta_slope::Float64
             max_slope_limit::Float64
             type_external::Int64
             min_depth::Float64
             max_depth::Float64
             min_discharge::Float64
            )
"""
mutable struct Settings
    commercial_diameters::Array{Float64,1}
    v_min::Float64
    v_max::Float64
    n::Float64
    roughness::Float64
    delta_slope::Float64
    max_slope_limit::Float64
    type_external::Int64
    min_depth::Float64
    max_depth::Float64
    min_discharge::Float64

    Settings(
        commercial_diameters = [
            0.225,
            0.25,
            0.35,
            0.4,
            0.5,
            0.6,
            0.80,
            1.0,
            1.20,
            1.5,
            2.0,
            2.5,
            3.0,
        ],
        v_min = 0.75,
        v_max = 10.0,
        n = 0.009,
        roughness = 0.0000015,
        delta_slope = 0.0001,
        max_slope_limit = 0.1,
        type_external = 1,
        min_depth = 0.5,
        max_depth = 5.0,
        min_discharge = 0.001,
    ) = new(
    commercial_diameters,
    v_min,
    v_max,
    n,
    roughness,
    delta_slope,
    max_slope_limit,
    type_external,
    min_depth,
    max_depth,
    min_discharge,
    )
end



function Base.show(s::Settings)
    fields = fieldnames(Settings)
    for f = 1:length(fields)
        field = fields[f]
        println("$field:")
        value = getfield(s, f)
        println("    $value")
    end
end

Base.show(io::IO, s::Settings) = Base.show(s::Settings)

"""
    read_network_file(file_path::String, sep::String=" ")
Return (manholes::Array{Float,2}, sections::Array{Float,2}).
"""
function read_network_file(file_path::String, sep::String = " ")

    if !isfile(file_path)
        # Input/Output Error if the file is not found.
        error("The file '$file_path' could not be found.")
    end

    manholes = Array{Float64}(undef, 0, 5)
    sections = Array{Float64}(undef, 0, 4)
    state = -1
    open(file_path) do f
        for l in eachline(f)
            if startswith(lowercase(l), "manhole")
                state = 0
            elseif startswith(lowercase(l), "pipe") || startswith(lowercase(l), "section")
                state = 1
            else
                if state == 0
                    manholes = vcat(manholes, parse.(Float64, split(l, sep))')
                elseif state == 1
                    sections = vcat(sections, parse.(Float64, split(l, sep))')
                end
            end
        end
    end
    return manholes, sections
end

"""
    map_unique(array::Array{Float64,1})
Return Dict(unique => count).
"""
function map_unique(array::Array{Float64,1})

    sort!(array)
    len = length(array)

    if len == 1
        return array, [1]
    end

    unique = Array{Float64}(undef, len)
    count = Array{Int64}(undef, len)
    uni_idx::Int64 = 1
    counter::Int64 = 1
    @inbounds @simd for idx = 1:(len-1)
        if array[idx] != array[idx+1]
            unique[uni_idx] = array[idx]
            count[uni_idx] = counter
            uni_idx += 1
            counter = 1
        else
            counter += 1
        end
    end

    if array[end] != array[end-1]
        unique[uni_idx] = array[end]
        count[uni_idx] = 1
    else
        unique[uni_idx] = array[end]
        count[uni_idx] = counter
    end
    return Dict(zip(unique[1:uni_idx], count[1:uni_idx]))
end

"""
    max_fill_ratio(diameter::Float64)
Return max_filling_ratio::Float64.
"""
function max_fill_ratio(diameter::Float64)
    max_filling_ratio::Float64 = 0.85
    if diameter <= 0.6
        max_filling_ratio = 0.7
    elseif 0.7 < diameter <= 1.5
        max_filling_ratio = 0.8
    end
    return max_filling_ratio
end

"""
    calculate_angle(diameter::Float64, y::Float64)
Return angle::Float64.
"""
function calculate_angle(diameter::Float64, y::Float64)
    return pi + 2 * asin((2 * y - diameter) / diameter)
end

"""
    calculate_flow_area(diameter::Float64, angle::Float64)
Return area::Float64.
"""
function calculate_flow_area(diameter::Float64, angle::Float64)
    return diameter^2 / 8 * (angle - sin(angle))
end

"""
    calculate_wetted_perimeter(diameter::Float64, angle::Float64)
Return wetted_perimeter::Float64.
"""
function calculate_wetted_perimeter(diameter::Float64, angle::Float64)
    return angle * diameter / 2
end

"""
    calculate_top_width(diameter::Float64, y::Float64)
Return width::Float64.
"""
function calculate_top_width(diameter::Float64, y::Float64)
    return diameter * cos(asin((2 * y - diameter) / diameter))
end

"""
    calculate_hydraulic_radius(diameter::Float64, angle::Float64)
Return hydraulic_radius::Float64.
"""
function calculate_hydraulic_radius(diameter::Float64, angle::Float64)
    return diameter / 4 * (1 - sin(angle) / angle)
end

"""
    calculate_tau(slope::Float64, radius::Float64)
Return tau::Float64.
"""
function calculate_tau(slope::Float64, radius::Float64)
    return 9.81 * 1000 * radius * slope
end

"""
    slope_manning(hydraulic_radius::Float64, velocity::Float64, n::Float64)
Return slope::Float64.
"""
function slope_manning(hydraulic_radius::Float64, velocity::Float64, n::Float64)
    return (velocity * n / (hydraulic_radius^(2 // 3)))^2
end

"""
    velocity_manning(hydraulic_radius::Float64, slope::Float64, n::Float64)
Return velocity.
"""
function velocity_manning(hydraulic_radius::Float64, slope::Float64, n::Float64)
    return 1 / n * (hydraulic_radius^(2 // 3)) * (slope^(1 // 2))
end

"""
    calculate_froude_number(area::Float64, velocity::Float64, top_width::Float64)
Return froude_number::Float64.
"""
function calculate_froude_number(area::Float64, velocity::Float64, top_width::Float64)
    return velocity / ((9.81 * area / top_width)^(1 // 2))
end

"""
    calculate_flow(diameter::Float64, slope::Float64, y::Float64, n::Float64)
Return flow::Float64.
"""
function calculate_flow(diameter::Float64, slope::Float64, y::Float64, n::Float64)
    angle = calculate_angle(diameter, y)
    area = calculate_flow_area(diameter, angle)
    radius = calculate_hydraulic_radius(diameter, angle)
    velocity = velocity_manning(slope, radius, n)
    return area * velocity
end

"""
    calculate_flow_fast(diameter::Float64, slope::Float64, y::Float64, n::Float64)
Return flow::Float64.
"""
function calculate_flow_fast(diameter::Float64, slope::Float64, y::Float64, n::Float64)
    angle = pi + 2 * asin((2 * y - diameter) / diameter)
    return (1 / angle)^(2 // 3) * (diameter^(8 // 3)) / (20.158737 * n) *
           ((angle - sin(angle))^(5 // 3)) * (slope^(1 // 2))
end

"""
    calculate_normal_depth(diameter::Float64, design_q::Float64,
                           slope::Float64, n::Float64,
                           precision_abs::Float64=0.000001)
Return normal_depth::Float64.
"""
function calculate_normal_depth(
    diameter::Float64,
    design_q::Float64,
    slope::Float64,
    n::Float64,
    precision_abs::Float64 = 0.000001,
)
    yni::Float64 = 0.0
    mfr = max_fill_ratio(diameter)

    ynf = diameter * mfr
    yn = (ynf + yni) / 2

    while abs(yni - ynf) > precision_abs

        flow = calculate_flow_fast(diameter, slope, yn, n)

        if flow > design_q
            ynf = yn
        else
            yni = yn
        end
        yn = (ynf + yni) / 2
    end
    return yn
end

"""
    check_constraints(diameter::Float64, velocity::Float64, tau::Float64, yn::Float64, froude::Float64, roughness::Float64)
Return ::Bool.
"""
function check_constraints(
    diameter::Float64,
    velocity::Float64,
    tau::Float64,
    yn::Float64,
    froude::Float64,
    roughness::Float64,
)
    """ Check hydraulic constraints
    """
    check::Bool = true

    if roughness > 0.0001
        if velocity > 5
            check = false
        end
    else
        if velocity > 10
            check = false
        end
    end
    # Check minimum speed and minimum shear stress constraints
    if (diameter >= 0.45) && (tau <= 2)
        check = false
    end

    # Check minimum shear stress constraints
    if (diameter < 0.45) && (velocity <= 0.75)
        check = false
    end

    # Check maximum filling ratio when shear stress < 2
    if (yn / diameter <= 0.1) && (tau < 2)
        check = false
    end

    # Check Froude's number and filling rate for quasi-critical flow (0.7 < froude > 1.5)
    if (0.7 < froude < 1.5) && (yn / diameter > 0.8)
        check = false
    end

    return check
end

"""
    calculate_terrain_slope_xy_distance(manholes::Array{Float64,2}, pipes::Array{Float64,2})
Return Dict(manhole => horizontal distance), Dict(manhole => terrain_slope).
"""
function calculate_terrain_slope_xy_distance(
    manholes::Array{Float64,2},
    pipes::Array{Float64,2},
)
    # prepare x, y, z coordinates per manhole
    manhole_coordinates = Dict{Float64,Int64}()
    @inbounds @simd for i = 1:size(manholes)[1]
        manhole_coordinates[manholes[i, 1]] = i
    end

    num_pipes = size(pipes)[1]
    horizontal_dist = Array{Float64}(undef, num_pipes)
    terrain_slope = Array{Float64}(undef, num_pipes)

    for i = 1:num_pipes
        idx_up = manhole_coordinates[pipes[i, 1]]
        idx_down = manhole_coordinates[pipes[i, 2]]
        horizontal_dist[i] = ((manholes[idx_up, 2] - manholes[idx_down, 2])^2 +
                              (manholes[idx_up, 3] - manholes[idx_down, 3])^2)^(1 // 2)
        terrain_slope[i] = (manholes[idx_down, 4] - manholes[idx_up, 4]) /
                           horizontal_dist[i]
    end
    return Dict(zip(pipes[:, 1], horizontal_dist)), Dict(zip(pipes[:, 1], terrain_slope))

end

"""
    prepare_hydraulics_data(settings::Settings)
Return min_slopes::Array, max_slopes::Array, filling_hydraulic_radius::Array, max_areas::Array.
"""
function prepare_hydraulics_data(settings::Settings)

    len = length(settings.commercial_diameters)
    # Prepare arrays for the hydraulic calculations
    min_slopes = Array{Float64}(undef, len)
    max_slopes = Array{Float64}(undef, len)
    filling_hydraulic_radius = Array{Float64}(undef, len)
    max_areas = Array{Float64}(undef, len)

    # calculating the max filling ratio and the min slopes
    for idx = 1:len
        # Hydraulic parameters for the maximum filling ratio of the pipe with a specific diameter
        diameter = settings.commercial_diameters[idx]
        y = max_fill_ratio(diameter) * diameter
        angle = calculate_angle(diameter, y)
        filling_hydraulic_radius[idx] = calculate_hydraulic_radius(diameter, angle)
        max_areas[idx] = calculate_flow_area(diameter, angle)

        # Calculate minimum and maximum slope for the given diameter
        min_slopes[idx] = slope_manning(
            filling_hydraulic_radius[idx],
            settings.v_min,
            settings.n,
        )
        max_slopes[idx] = slope_manning(
            filling_hydraulic_radius[idx],
            settings.v_max,
            settings.n,
        )
    end
    return min_slopes, max_slopes, filling_hydraulic_radius, max_areas
end



"""
    Network(design_discharge::Dict{Float64, Float64}
             manholes_outer::Array{Float64,1}
             manhole_last::Array{Float64,1}
             pipe_map::Dict{Float64, Float64}
             manhole_inflow_map::Dict{Float64, Float64}
             count_manhole_inflow::Dict{Float64, Int64}
             elevation_map::Dict{Float64, Float64}
             elevation_invert_map::Dict{Float64, Float64}
             xy_distance::Dict{Float64, Float64}
             slopes_terrain::Dict{Float64, Float64}
             diameter_idx_tracker::Dict{Float64, Int64}
             diameter_tracker::Dict{Float64, Float64}
             pumps::Dict{Float64,  Tuple{Float64, Float64}}
             deepened::Dict{Float64, Tuple{Float64, Float64}}
             chosen_slopes::Dict{Float64, Float64}
             min_slopes::Array{Float64, 1}
             max_slopes::Array{Float64, 1}
             filling_hydraulic_radius::Array{Float64, 1}
             max_areas::Array{Float64, 1}
             raw::Bool
            )
"""
mutable struct Network

    design_discharge::Dict{Float64,Float64}
    manholes_outer::Array{Float64,1}
    manhole_last::Array{Float64,1}
    pipe_map::Dict{Float64,Float64}
    manhole_inflow_map::Dict{Float64,Float64}
    count_manhole_inflow::Dict{Float64,Int64}
    elevation_map::Dict{Float64,Float64}
    elevation_invert_map::Dict{Float64,Float64}
    plain_distance::Dict{Float64,Float64}
    slopes_terrain::Dict{Float64,Float64}
    diameter_idx_tracker::Dict{Float64,Int64}
    diameter_tracker::Dict{Float64,Float64}
    pumps::Dict{Float64,Tuple{Float64,Float64}}
    deepened::Dict{Float64,Tuple{Float64,Float64}}
    slopes_chosen::Dict{Float64,Float64}
    slopes_min::Array{Float64,1}
    slopes_max::Array{Float64,1}
    filling_hydraulic_radius::Array{Float64,1}
    max_areas::Array{Float64,1}
    raw::Bool

end

function Base.show(nw::Network)
    if nw.raw
        println("Raw network, use design_network! method to construct!")
    else
        println("Designed network!")
    end
end

Base.show(io::IO, nw::Network) = Base.show(nw::Network)


"""
    init_network(manholes::Array{Float64,2}, pipes::Array{Float64,2}, settings::Settings)
Returns network::Network
"""
function init_network(
    manholes::Array{Float64,2},
    pipes::Array{Float64,2},
    settings::Settings,
)

    # design discharge for each pipe
    design_discharge = Dict(zip(pipes[:, 1], pipes[:, 4]))

    # find all start nodes
    manholes_outer = pipes[pipes[:, 3].==settings.type_external, 1]

    # find last manhole
    manhole_last = manholes[manholes[:, 5].==1, 1]

    # map nodes for fast access
    pipe_map = Dict(zip(pipes[:, 1], pipes[:, 2]))

    # map repeated downstream ids and its frequency (No. of incoming pipes) per manhole
    manhole_inflow_map = map_unique(pipes[:, 2])

    # prepare inflow count
    count_manhole_inflow = Dict([(k, 0) for k in keys(manhole_inflow_map)])

    # prepare elevation dictionary
    elevation_map = Dict(zip(manholes[:, 1], manholes[:, 4]))

    # prepare elevation dictionary
    elevation_invert_map = copy(elevation_map)

    # get terrain slopes and xy - distances
    plain_distance, slopes_terrain = calculate_terrain_slope_xy_distance(manholes, pipes)

    # prepeare diameter tracker
    diameter_idx_tracker = Dict([(k, 1) for k in keys(pipe_map)])

    # prepare dict to store chosen diameters
    diameter_tracker = Dict{Float64,Float64}()

    for mh in manhole_last
        # add break criteria for reaching last mh:
        count_manhole_inflow[mh] = -1

        # add fake diameter for last manhole
        diameter_idx_tracker[mh] = 1
    end

    # prepare dict for pump info key: mh, value: (bottom old, bottom new)
    pumps = Dict{Float64,Tuple{Float64,Float64}}()

    # deepened manholes because of steep terrain key: mh, value: (min_depth, new_depth)
    deepened_manholes = Dict{Float64,Tuple{Float64,Float64}}()

    # to keep track of the chosen slopes. key: manhole_up, value: slope
    slopes_chosen = Dict{Float64,Float64}()

    # calculate static hydraulic values
    slopes_min,
    slopes_max,
    filling_hydraulic_radius,
    max_areas = prepare_hydraulics_data(settings)

    return Network(
        design_discharge,
        manholes_outer,
        manhole_last,
        pipe_map,
        manhole_inflow_map,
        count_manhole_inflow,
        elevation_map,
        elevation_invert_map,
        plain_distance,
        slopes_terrain,
        diameter_idx_tracker,
        diameter_tracker,
        pumps,
        deepened_manholes,
        slopes_chosen,
        slopes_min,
        slopes_max,
        filling_hydraulic_radius,
        max_areas,
        true,
    )
end




"""
    find_best_slope(design_discharge::Float64, ideal_slope::Float64, diameter_idx::Int64,
                    min_slopes::Array{Float64,1}, max_slopes::Array{Float64,1},
                    filling_hydraulic_radius::Array{Float64,1}, max_areas::Array{Float64,1},
                    settings::Settings)
Return slope::Float64, diameter_idx::Int64.
"""
function find_best_slope(
    design_discharge::Float64,
    ideal_slope::Float64,
    diameter_idx::Int64,
    network::Network,
    settings::Settings,
)

    if design_discharge < settings.min_discharge
        return 0, 0
    end

    @inbounds for idx = diameter_idx:length(settings.commercial_diameters)
        diameter = settings.commercial_diameters[idx]
        fill_hyd_radius = network.filling_hydraulic_radius[idx]
        max_area = network.max_areas[idx]
        min_slope = network.slopes_min[idx]
        max_slope = network.slopes_max[idx]


        if (min_slope <= ideal_slope <= max_slope)
            start_slope = ideal_slope
        else
            start_slope = min_slope
        end

        velocity = settings.v_min

        max_discharge::Float64 = 0.0
        @inbounds for slope = start_slope:settings.delta_slope:min(
            settings.max_slope_limit,
            max_slope,
        )
            if (velocity > settings.v_max)
                break
            end
            # make sure discharge fits through the pipe
            max_discharge = max_area * velocity_manning(fill_hyd_radius, slope, settings.n)

            if design_discharge < max_discharge
                # calculate the real flow depth
                normal_depth = calculate_normal_depth(
                    diameter,
                    design_discharge,
                    slope,
                    settings.n,
                )

                # recalculate geometric parameters
                angle = calculate_angle(diameter, normal_depth)
                radius = calculate_hydraulic_radius(diameter, angle)
                area = calculate_flow_area(diameter, angle)

                # recalculate hydraulic parameters
                velocity = velocity_manning(radius, slope, settings.n)
                tau = calculate_tau(slope, radius)
                top_width = calculate_top_width(diameter, normal_depth)
                froude = calculate_froude_number(area, velocity, top_width)

                # check hydraulic constraints
                checked = check_constraints(
                    diameter,
                    velocity,
                    tau,
                    normal_depth,
                    froude,
                    settings.roughness,
                )

                if checked
                    return slope, idx
                end
            end
        end

        ##### must be after loop!
        if design_discharge < max_discharge
            return 0, 0
        end
        ######
    end
end

"""
    find_pipe_configuration(mh_up::Float64,
                            mh_down::Float64,
                            ideal_slope::Float64,
                            is_outer_pipe::Bool,
                            network::Network,
                            settings::Settings
                            )
Return choosen_diameter::Float64
"""
function find_pipe_configuration(
    mh_up::Float64,
    mh_down::Float64,
    ideal_slope::Float64,
    choosen_diameter::Float64,
    is_outer_pipe::Bool,
    network::Network,
    settings::Settings,
)
    @fastmath slope, dia_idx = find_best_slope(
        network.design_discharge[mh_up],
        ideal_slope,
        network.diameter_idx_tracker[mh_up],
        network,
        settings,
    )
    # if design dis is close to zero!
    if (slope == 0) & (dia_idx == 0)
        network.slopes_chosen[mh_up] = 0
        network.diameter_idx_tracker[mh_up] = 0
        network.elevation_invert_map[mh_up] = network.elevation_map[mh_up] -
                                              settings.min_depth
        network.elevation_invert_map[mh_down] = network.elevation_map[mh_down] -
                                                settings.min_depth
    else

        delta_h = slope * network.plain_distance[mh_up]

        network.slopes_chosen[mh_up] = slope
        network.diameter_idx_tracker[mh_up] = dia_idx


        choosen_diameter = settings.commercial_diameters[dia_idx]
        if is_outer_pipe
            network.elevation_invert_map[mh_up] = network.elevation_map[mh_up] -
                                                  settings.min_depth - choosen_diameter
        end

        if network.diameter_idx_tracker[mh_up] > network.diameter_idx_tracker[mh_down]
            network.diameter_idx_tracker[mh_down] = network.diameter_idx_tracker[mh_up]
        end

        elevation_invert_end = network.elevation_invert_map[mh_up] - delta_h

        # if the pipe slope is too shallow therefore not
        # reaching the minimal depth of the downstream manhole
        if network.elevation_map[mh_down] - elevation_invert_end < (settings.min_depth +
                                                                    choosen_diameter)
            elevation_invert_end = network.elevation_map[mh_down] - settings.min_depth -
                                   choosen_diameter
            new_depth = elevation_invert_end + delta_h
            network.deepened[mh_up] = (network.elevation_invert_map[mh_up], new_depth)
            network.elevation_invert_map[mh_up] = new_depth
        end

        # update depth of manhole if new connection depth is deeper
        temp_elevation_invert_end = network.elevation_invert_map[mh_down] # needed for pump. if one connection needs a pump and the other one does not!
        if elevation_invert_end < network.elevation_invert_map[mh_down]
            network.elevation_invert_map[mh_down] = elevation_invert_end
        end

        # adding a pump more cases need to be considered!
        if network.elevation_map[mh_down] -
           network.elevation_invert_map[mh_down] > settings.max_depth
            new_start_depth = network.elevation_map[mh_down] - settings.min_depth -
                              choosen_diameter + delta_h
            network.pumps[mh_up] = (network.elevation_invert_map[mh_up], new_start_depth)
            network.elevation_invert_map[mh_up] = new_start_depth
            network.elevation_invert_map[mh_down] = min(
                new_start_depth - delta_h,
                temp_elevation_invert_end,
            )

        end
    end
    return choosen_diameter
end


"""
    walk_graph(manholes::Array{Float64,2}, pipes::Array{Float64,2}, settings::Settings)
Return elevation_invert::Dict, chosen_slopes::Dict, pumps::Dict, deepened::Dict, diameter_tracker::Dict.
"""
function design_network(network::Network, settings::Settings)

    if network.raw

        choosen_diameter::Float64 = 0.0
        # iterate all outer manholes!
        for mh_up in network.manholes_outer
            mh_down = network.pipe_map[mh_up]
            network.count_manhole_inflow[mh_down] += 1

            ideal_slope = network.slopes_terrain[mh_up]

            choosen_diameter = find_pipe_configuration(
                mh_up,
                mh_down,
                ideal_slope,
                choosen_diameter,
                true,
                network,
                settings,
            )

            # keep moving along the path until you reach a node that has multiple inflows
            while network.count_manhole_inflow[mh_down] == network.manhole_inflow_map[mh_down]

                mh_up = mh_down
                mh_down = network.pipe_map[mh_up]
                network.count_manhole_inflow[mh_down] += 1

                ideal_slope = (network.elevation_invert_map[mh_up] -
                               network.elevation_map[mh_down] +
                               settings.min_depth + choosen_diameter) /
                              network.plain_distance[mh_up]

                choosen_diameter = find_pipe_configuration(
                    mh_up,
                    mh_down,
                    ideal_slope,
                    choosen_diameter,
                    false,
                    network,
                    settings,
                )

            end
        end

        # convert diameter indexes to actual diameters
        for (mh, idx) in network.diameter_idx_tracker
            if idx == 0
                network.diameter_tracker[mh] = 0.0
            else
                network.diameter_tracker[mh] = settings.commercial_diameters[idx]
            end
        end
        network.raw = false
        nothing
    else
        println("The network has already been designed!")
    end
end



end
